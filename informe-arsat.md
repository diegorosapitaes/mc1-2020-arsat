# Principales organismos involucrados en la construcción de satélites argentinos:

### INVAP:
 Es una empresa referente en proyectos tecnológicos a nivel mundial y protagonista del desarrollo en Argentina. Desde hace cuarenta años desarrolla sistemas tecnológicos de alto valor agregado, tanto para satisfacer necesidades nacionales como para insertarse en mercados externos a través de la exportación. Sus principales actividades se centran en las áreas Nuclear, Espacial, Defensa y Seguridad, Industrial y Comunicaciones. INVAP ha diseñado y fabricado reactores de investigación en distintos lugares del mundo, satélites científicos para la observación terrestre, satélites de telecomunicaciones, sistemas de radares para control del tránsito aéreo, para defensa y meteorológicos, centros de medicina nuclear, entre otros importantes desarrollos.

### CONAE:
 La Comisión Nacional de Actividades Espaciales, es la agencia del Gobierno de Argentina Responsable del Plan Nacional Espacial de Argentina. Actualmente es el organismo competente para entender, diseñar, ejecutar, controlar, gestionar y administrar proyectos, actividades y emprendimientos en materia **espacial** en todo el ámbito de la República Argentina. En conjunto con INVAP y asociándose principalmente con agencia de los Estados Unidos, (NASA), provee la plataforma satelital y la mayoría de los instrumentos de dichos satélites.
***
# Línea de tiempo: ARSAT

+ Agosto de 2004: El gobierno decidió quitar la licencia a Nahuelsat para operar la posición de 81 grados oeste.
+ Plan Espacial Nacional 2004 – 2015: Se crea a través de la Comisión Nacional de Actividades Espaciales (CONAE). El mismo se formuló para conducir al país al dominio integral de esta tecnología, con el objeto que la información generada desde el espacio pueda satisfacer las necesidades de determinadas áreas del aparato productivo nacional.
+ El 17 de Octubre de 2005, el Anik E2 fue trasladado a la órbita de 81 grados oeste y se pudo conservar la asignación. El 19 de octubre el satélite realizó la primera transmisión y aseguró la adjudicación de la órbita a la Argentina.
+ En 2006 se dictó la Ley 26.092, que creó la Empresa Argentina de Soluciones Satelitales Sociedad Anónima (ARSAT).
+ A partir de 2007 ARSAT e INVAP trabajaron juntos para poder cumplir con la misión de construir el primer **satélite de comunicaciones geoestacionario** de fabricación nacional. El proyecto se dividió en cuatro fases: la primera fue llamada de ingeniería preliminar; la segunda se puso en práctica la ingeniería de detalle; la tercera etapa fue de fabricación, integración y ensayos; y la última consistió en lanzamiento y puesta en órbita del satélite, realizado el 16 de octubre de 2014.
+ A partir de 2010 INVAP comenzó a fabricar el ARSAT-1 en San Carlos de Bariloche.
+ En 2013 se realizó el ensamble de todas sus piezas. Desde ese momento hasta 2014 se realizaron los ensayos en tierra, para lo cual se construyó el **Centro de Ensayos de Alta Tecnología (CEATSA)**. Finalmente, su lanzamiento fue realizado el 16 de octubre de 2014 en Guayana Francesa.
+ El 10 de diciembre de 2014 se realiza la primera transmisión del satélite, durante los festejos por el 31 aniversario del retorno a la democracia.
+ El 30 de septiembre de 2015 se lanzó el ARSAT-2. Otro satélite de comunicaciones geoestacionario íntegramente diseñado, construido y ensayado por la empresa argentina INVAP.
+ Hacia fines del 2015, estaba iniciada la etapa de construcción del ARSAT-3, proyectado para abarcar todo el continente americano. Sin embargo, en marzo de 2016 el entonces titular de ARSAT, anunció la suspensión de su construcción.
+ Para fines de 2015, 11,7 millones de argentinos usaban al menos un servicio de ARSAT y la empresa proyectaba que en 2016 serían más de 21 millones los usuarios.
+ En enero de 2016, se denunció el despido masivo e intempestivo de trabajadores, que atribuyeron la situación a persecución ideológica y no coincidir con el lineamiento político de las nuevas autoridades.
+ En marzo de 2016 las nuevas autoridades anunciaron la decisión de interrumpir la construcción del ARSAT-3 debido a problemas de financiamiento, a pesar de que la ley prevé que la financiación de dicho satélite provenga de la venta de los servicios de ARSAT-1 y ARSAT-2.

(https://es.wikipedia.org/wiki/ARSAT)
(https://drive.google.com/drive/folders/1a7Rm84OV-j_8jS_2HWQ8LQBiBkrr7pKd)

***

# Descripción técnica e instrumental de los Satélites ARSAT-1 ARSAT-2:

Los Satélites ARSAT-1 y ARSAT-2 se basan en la plataforma ARSAT-3K9, cuya arquitectura base es similar a la plataforma Spacebus 3000B2 desarrollada por el fabricante europeo Thales Alenia Space (TAS). Se trata de una plataforma que ofrece grandes ventajas para un país que se inicia en la construcción de un satélite geoestacionario.
El diseño del satélite está basado en una propulsión química bi-propelente, permitiendo una selección flexible de lanzador. La masa de la carga útil, equipamiento que lleva el satélite para poder cumplir con su misión específica, puede llegar a ser de 350 kg y la potencia hasta 3,4 kW (End of Life). Es un diseño dúctil que permite acomodar una amplia gama de misiones y cargas útiles en telecomunicaciones. ARSAT cuenta con la propiedad intelectual y los derechos de comercialización de la mencionada plataforma.

**Dimensiones (sin apéndices):** 2,0 m x 1,8 m 2,9 m

**Masa (incluye carga útil):** En seco 1300 kg, Húmedo 3000 kg

**Duración de la transferencia a órbita GEO:** 10 días aprox.

**Tipo de carga útil** Cualquier de telecomunicaciones que respete la envolvente de potencia y masa de la plataforma (bandas C, Ku, Ka).

**Compatibilidad con lanzador** Inyección GTO para Ariane V, Soyuz (Kourou) y Falcon 9.

**Vida útil:** 15 años.

**Posicionamiento de antenas:** 1 en banda Ku, 1 gregoriana, 1 en banda C

**Potencia disponible de la carga útil:** 3400 W

**Confiabilidad** 75% por 15 años, lo cual equipara los niveles estándar del mercado.

### Satélite ARSAT-1

El satélite ARSAT-1 pesa 3 toneladas. Su cuerpo principal (sin apéndices) mide 2 m por 1,8 m por 2,9 m. Una vez en órbita, se despliegan los paneles solares (con los que alcanza los 16,,42 m de envergadura) y una antena de comunicaciones de 2 m de diámetro.

Desarrollado en base a la plataforma estándar de 3 kW de ARSAT, el ARSAT-1 transmite en banda Ku y cuenta con una carga útil de 1152 MHz repartida en 12 transponders de 36 MHz, 8 de 54 MHz y 4 de 72 MHz.

#### Cobertura de diseño de huella de ARSAT-1

**Lanzamiento:** 16 de octubre de 2014 (Arianspace).

**Constructor:** INVAP – ARSAT (Argentina).

**Vida útil:** 15 años.

**Ángulo de elevación:** Mayor a 25°

**Polarización:** Lineal (Crospol).

**Cobertura:** Argentina – Banda Ku – 1152MHz PIRE: 44 a 52,7 dBw. Y G/T:0 a 6 dBk

### Satélite ARSAT-2

Al igual que el ARSAT-1, el segundo de la flota tiene un cuerpo principal de aproximadamente 2 m x 1,8 m x 2,9 m (sin los paneles solares ni las antenas) y con los paneles desplegados mide 16,42 m entre sus extremos. La única diferencia dimensional es el mayor número de antenas del ARSAT-2: tiene tres (dos desplegables y una fija o gregoriana), mientras que el ARSAT-1 tiene solo una antena.

La antena fija del ARSAT-2 transmite en banda Ku, mientras que las desplegables lo hacen una en banda Ku y la otra en banda C.

De acuerdo a la capacidad de la carga útil de la plataforma estándar de 3 kW de ARSAT, el segundo satélite de la flota se compone de los siguientes transponders:

+ Cuatro de 72 MHz y dos de 88 MHz en banda C para cobertura latinoamericana.
+  Ocho de 36 MHz y dos de 72 MHz en banda Ku para cobertura sudamericana sin Amazonas.
+ Dos de 72 MHz en banda Ku para cobertura norteamericana sin México.
+ Ocho de 36 MHz en banda Ku conmutables entre Sud y Norte América.

#### Cobertura y diseño de huella de ARSAT-2

**Lanzamiento:** Finales de 2015 (Arianspace).

**Constructor:** INVAP – ARSAT (Argentina)

**Vida útil:** 15 años

**Ángulo de elevación:** Mayor a 25°

**Polarización:** Lineal (Crospol)

**Cobertura:** 1) Sudamérica s/Brasil – Banda Ku – 576 MHz PIRE: 46 a 49 dBw. Y G/T: -1 a 3 dBK. 2) Norteamérica – Banda Ku – 144 MHz PIRE: 44 a 50 dBw. Y G/T: -2 a 3 dBK. 3) Hispanoamérica – Banda C – 320 MHz PIRE: 46 a 49 dBw.

(https://drive.google.com/drive/folders/1a7Rm84OV-j_8jS_2HWQ8LQBiBkrr7pKd)

# Ensayos ambientales y mediciones para satélites GEO en CEATSA

- **Ensayo de Termo vacío:**

En la cámara de termo-vacío se simula el vacío del espacio exterior y se cicla al satélite a temperaturas altas y bajas. Es un recipiente totalmente hermético donde se somete a sus distintos equipamientos a las condiciones reales de operación que tendrá durante su vida útil. Este particular laboratorio de doble propósito puede generar condiciones de vacío en el orden de 10-7 Torr, es decir, 7600 millones de veces menos que la presión atmosférica a nivel del mar, y temperaturas desde -150°C hasta 170 °C. Las diferentes temperaturas de los equipamientos son medidas por 100 termocuplas conectadas a un sistema de control de datos. Las condiciones de vacío y temperatura son pre-programables a la medida de cada cliente, ampliando de gran manera la diversidad de experimentos que pueden realizarse en la cámara.

- **Ensayo de vibraciones mecánicas:**

En el shaker se simulan las intensas vibraciones que soporta el satélite en el lanzamiento. El sistema de vibración somete a los equipamientos a vibraciones sinusoidales, casi estáticas o impulsivas sobre los tres ejes del cuerpo. Esto es esencial para el aseguramiento del satélite, ya que nos indica la verdadera resistencia que tendrá. Se puede validar también su diseño estructural. Para lograrlo, se colocan una serie de acelerómetros y otros instrumentos de medición en el objeto a ser testeado, pudiendo registrar el estrés y las deformaciones elásticas que sufre durante los ensayos.

- **Ensayo de vibraciones acústicas:**

El ensayo acústico reproduce el altísimo nivel de sonido que recibe el satélite durante el despegue. El experimento es denominado prueba de sonido de campo directo (Direct Field Sound Acoustic Test, DFAT) y consiste en someter al satélite a distintos sonidos emitidos por parlantes estratégicamente ubicados simulando los del lanzamiento y los que sufrirá durante toda su vida útil. Una serie de acelerómetros y otros instrumentos de medición registran el estrés y las deformaciones elásticas que sufre durante los ensayos.

- **Ensayo de propiedades de masa:**

En el ensayo de propiedades de masa se miden pesos de hasta 4 T. con una precisión de +/- 100 g. Específicamente, existe una variedad de equipamiento destinado a evaluar la masa, el centro de gravedad y el momento de inercia del objeto a ser ensayado. La expectativa de vida de ciertas piezas complejas, incluidos su performance y su deterioro programado, son consecuencias directas de sus propiedades físicas. Estas mediciones sirven para determinar los límites de la calidad garantizada para una adecuada operación. Este ensayo es particularmente importante en la industria espacial, dado que la correcta posición final, el control de actitud y la correcta la inyección en la órbita de transferencia en el momento del LEOP determinan el éxito de la misión.

- **Ensayo de performance de antena:**

En el scanner de la cámara anecoica se mide la performance de la antena de comunicaciones del satélite evaluando su capacidad para transmitir correctamen te desde órbita geoestacionaria. El scanner plano horizontal mide el patrón de radiación de la antena, su eficiencia, sus propiedades direccionales, su fase (por holografía) y sus polarizaciones Cpol y Xpol. En el área de escaneo, una reja de medición es llevada de un lado a otro tomando distintos valores de módulo y fase de la radicación de las antenas. En cada medición, el software de adquisición de datos guarda en un archivo particular los valores de la posición del sensor (X, Y, Z) sumados a los de módulo y fase ya nombrados.

- **Otros ensayos:**

En la cámara anecoica también se pueden realizar experimentos de compatibilidad electromagnética (EMC) y de distintas emisiones de equipamientos electrónicos.

https://drive.google.com/drive/folders/1a7Rm84OV-j_8jS_2HWQ8LQBiBkrr7pKd

# Estaciones terrenas

Las estaciones terrenas de ARSAT comprenden todos los sistemas que hacen a la comunicación satelital. Actualmente son dos, la Estación Terrena Benavídez (ETB) y la Estación Terrena Córdoba (ETC).

**Estación Terrena Córdoba (ETC)**

Actualmente la ETB quedó integrada dentro de un complejo tecnológico más grande con el que ARSAT puede desarrollar otras tareas que le han sido encomendadas por el Estado Nacional como es el caso de la implementación y operación de la Red Federal de Fibra Óptica, el Centro Nacional de Datos, y la plataforma tecnológica de la TDA.

**Estación Terrena Benavídez (ETB)**

Cuenta con instalaciones que le permiten brindar una amplia gama de servicios satelitales.

## Servicios satelitales brindados desde la Estación Terrena Benavídez

### Operación satelital:

- **Operaciones de control satelital para LEOP (Low Earth Orbit Phase10)**

Soporta las operaciones satelitales cuando se lanza un satélite e integra una red de estaciones en todo el mundo para este propósito. Esencialmente, se encarga de recibir y procesar la telemetría del satélite, procesar y mandar comandos al satélite y medir donde se encuentra para la predicción orbital y diseño de maniobras. Pueden ser servicios como el realizado para ARSAT-1 y a realizarse para los futuros satélites ARSAT o servicios para terceros operadores que contratan a la ETB para sus propios lanzamientos. La ETB integra la red de LEOP del operador Telespazio.

- **Operaciones de control satelital en órbita geoestacionaria (Goestationary Orbit, GO)**

Recibe y procesa la telemetría del satélite; realiza mediciones para conocer dónde se encuentra el satélite (predicción orbital); realiza mediciones para diseño de maniobras cuando se encuentra en su posición orbital definitiva; y procesa y envía comandos.

- **Operaciones de testeos en órbita (In Orbit Test, IOT)**

Permite, una vez que el satélite ha llegado a su posición orbital, realizar las pruebas en su carga útil y verificar que los canales que luego se ofrecerán a nuestros clientes, están en buenas condiciones para empezar a operar comercialmente. Operaciones de monitoreo de portadoras (Carrier System Monitoring, CSM). Permite monitorear las señales de los clientes en el satélite, que permiten comprobar el buen uso que se esté dando a la capacidad satelital y detectar posibles anomalías o interferencias.

### Telepuerto:

- **Interface de RF y antenas para acceder a los satélites**

Servicio utilizado para señales de TV, para estaciones remotas de TDA terrestre; señales de redes VSAT propias; y tournarounds, servicio que implica recibir una señal de un satélite para retransmitirla a otro satélite.

### Hosting o housing:

Capacidad brindada a terceros operadores o usuarios externos a ARSAT para procesar y transmitir sus propias señales.
Servicios para hosting CSM para operadores y hosting de red VSAT para clientes.

https://drive.google.com/drive/folders/1a7Rm84OV-j_8jS_2HWQ8LQBiBkrr7pKd

# Discusión sobre la importancia de la soberanía espacial y de telecomunicaciones en Argentina

Durante muchos años, Argentina estuvo luchando para la obtención de la soberanía espacial, realizando varios proyectos, como lo fueron los proyectos SAC, los proyectos ARSAT y los proyectos SAOCOM, entre otros. Estos dos últimos mencionados siguen llevándose acabo en la actualidad y recientemente se empezó con la planificación y gestión del nuevo satélite ARSAT-3.

Desde el primer satélite argentino enviado al espacio en 1990 (el satélite Lusat 1), Argentina ya ha puesto en órbita 17 satélites. Este año se esperaba que se lanzara el satélite de observación terrestre SAOCOM 1B, pero por la pandemia del Covid-19 el lanzamiento se pospuso.

La CONAE (Comisión Nacional de Actividades Espaciales) es la encargada de las planificaciones y las gestiones de los satélites nacionales y la responsable del Plan Nacional Espacial de Argentina. La importancia de la existencia de dicha agencia es realizar más satélites con el paso del tiempo, brindando mayor independencia sobre otras naciones y ofreciendo a los argentinos mayor información y beneficios con los servicios que brindan.

Alcanzar la soberanía espacial significa que un país es capaz de diseñar, construir y poner en órbita sus propios satélites, otorgando además soberanía tecnológica, de telecomunicaciones y científica. La realización de proyectos e inventos en una nación es fundamental tanto para los habitantes como para los gobiernos. Con la invención de proyectos nacionales, los gobiernos pueden independizarse de las prestaciones otorgadas por países extranjeros, si bien para la realización de los mismos se debe invertir una suma considerable de capital.

Por otra parte, se logra una manipulación privada de los datos y contenidos que se generan, ya que es el propio país el que opera en este caso los satélites, que almacenan cierta cantidad de información. La CONAE tiene laboratorios de ensayos, de mediciones y centros de controles para el monitoreo de los satélites que están en la actualidad orbitando la Tierra.

En cuanto al sector económico, el uso de satélites propios para ofrecer un mejor servicio de telecomunicaciones (en el caso de los ARSAT) o bien para estudios terrestres (en el caso de los SAOCOM) es fundamental para perfeccionar actividades económicas en el país, como son las comunicaciones y la agricultura en el campo. A su vez, el programa “Tronador II” tiene como objetivo la creación de un cohete para poner en órbita los satélites, permitiendo un ahorro en alquiler de cohetes extranjeros y una comercialización a terceros.

Con respecto a los argentinos, la realización de estos proyectos fortalece a la matriz productiva en el país ya que prestan servicios tanto de empresas estatales, empresas privadas y también de PyMes. Además, se le da mucha importancia a los ingenieros y profesionales científicos, que pueden transitar momentos complicados ante la falta de proyectos científicos.

# Nuevo ARSAT-3

El ARSAT-3 es un nuevo proyecto aeroespacial de las empresas de “INVAP” y “ARSAT”, estimando la finalización de su construcción para el año 2023 y siguiendo con la linea de los anteriores satélites ARSAT-1 y ARSAT-2. El objetivo de este nuevo satélite es brindar servicios de internet principalmente en todo Argentina, aunque no se descarta la idea de implementarlo en países limítrofes. A su vez, el nuevo proyecto aeroespacial se enfoca en la conexión de red en lugares topográficos donde no es sencillo llegar estos servicios mediante fibra óptica.

Durante los primeros meses, la idea es definir el tipo de satélite que se necesitará, junto con los servicios que se brindarán, para luego realizar las etapas de ingeniería, construcción, ensayos y por último el lanzamiento al espacio. El propósito que tienen las empresas es tener el satélite finalizado en 36 meses de trabajo y listo para estar operando en el espacio.

Para el ARSAT-3 se utilizarán nuevas tecnologías que no estaban disponibles en las construcciones de los anteriores satélites. Una de éstas tecnologías será la implementación de un equipamiento llamado HTS (High-Throughput Satellite), el cual es un sistema de alto rendimiento para ofrecer servicios de internet. Otra innovación que se realizará es el establecimiento de un sistema de propulsión eléctrica ya que en los anteriores modelos se utilizaba combustible.

El uso de estas dos tecnologías le otorgan al satélite ser bastante más livianos y brindar un mejor desempeño. Además de esto, la disminución de la masa conlleva un menor costo de lanzamiento y se comienza a utilizar la tecnología que predomina en la actualidad a nivel mundial.

**Biliografía**: [Link](https://www.youtube.com/watch?v=-VOmSw2HBTc)

# Tecnología HTS

Los satélites de alto rendimiento (HTS) son  diferentes en términos de diseño y capacidad en comparación con los satélites tradicionales, ya que esta nueva tecnología utiliza la misma cantidad de frecuencia orbital asignada y aprovecha las ventajas de reutilizar las frecuencias y haces angostos (spot). Al realizar esto, se logra una reducción significativa en el costo por bit transportado, independientemente de la opción de espectro.

El desarrollo de los sistemas HTS depende de variables como la región geográfica, la arquitectura del cliente y las aplicaciones específicas, además de ciertos elementos centrales a considerar. Uno de estos es el rendimiento que está constituido por el ancho de banda y la eficiencia. Dependiendo de los servicios ofrecidos a los clientes, se decidirá maximizar uno de los dos si es que no se pueden realizar ambos aspectos.

Con respecto a la eficiencia en el diseño técnico, los haces (spot) pueden aumentarla. Sin embargo colocando varios de ellos muy próximos entre sí pueden aumentar la interferencia y de esta manera disminuir la eficiencia. Por lo tanto, para aumentar la eficiencia debe haber mayor distancia entre los haces, aunque esto también reduce la reutilización de las frecuencias.

Con respecto a la eficiencia en el diseño técnico, los haces (spot) pueden aumentarla. Sin embargo colocando varios de ellos muy próximos entre sí pueden aumentar la interferencia y de esta manera disminuir la eficiencia. Por lo tanto, para aumentar la eficiencia debe haber mayor distancia entre los haces, aunque esto también reduce la reutilización de las frecuencias.

Por otra parte, la cobertura brindada por estos satélites y la arquitectura de Red son elementos importantes. La superficie cubierta está limitada con el número de haces que se pueden otorgar y el tamaño de dichos haces depende de la frecuencia seleccionada. En cuanto a la red, los satélites HTS presentan arquitecturas abiertas y cerradas. Las arquitecturas abiertas se utilizan mayormente para maximizar el rendimiento en aplicaciones en lugares específicos y son compatibles con muchas topologías de red comunes. Con respecto a las arquitecturas cerradas, éstas abarcan la banda ancha para los consumidores y trunking (sistemas agrupados).

**Bibliografía**:
[Link 1](https://www.youtube.com/watch?v=CW27Uv9W2Bk)
[Link 2](http://satcompost.com/definiendo-satelites-de-alto-throughput-hts/)

# Propulsión eléctrica

Durante mucho tiempo, la propulsión era realizada con combustible, y esto generaba una carga considerable en el peso en los cohetes. Por esta razón, se logró disminuir dicho problema con la innovación de la propulsión eléctrica. Este nuevo sistema utiliza energía eléctrica, generando campos electromagnéticos para expulsar propelente (masa de reacción) a alta velocidad. En general, los propulsores eléctricos brindan una mayor duración de empuje por unidad de masa de propelente que los cohetes a combustible ya que éstos últimos están limitados a la cantidad de energía que disponen.

Este sistema de propulsión eléctrica es una tecnología utilizada para cohetes en el espacio ya que no es un método recomendado para lanzamientos desde la superficie terrestre. Esto es debido a que el empuje brindado por este tipo de propulsión es demasiado débil.

En la actualidad existen diversos tipos de sistemas de  propulsión eléctrica, como son los ejemplos de propulsión electrostática (aplicación de un campo eléctrico estático en la dirección de la aceleración), propulsión electromagnética (iones acelerados por la fuerza de Lorentz o por un campo electromagnético donde el campo eléctrico no está en la dirección de la aceleración) y propulsión electrotermal (se utilizan campos electromagnéticos para generar un plasma, aumentando la temperatura del propulsor en masa), entre otros.

**Bibliografía**: [Link](https://es.wikipedia.org/wiki/Retropropulsi%C3%B3n_espacial_el%C3%A9ctrica)