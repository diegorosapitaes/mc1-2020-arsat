### Satélites argentinos ARSAT-1, 2 y 3 aspectos técnicos, instrumental, propósito e importancia.

Realizar un trabajo de investigación conjunto en se que se haga un abordaje completo de los siguientes temas:

* Breve descripción de los principales organismos involucrados en la construcción de satélites argentinos.
* Línea de tiempo con los principales hitos del ARSAT
* Descripción técnica e instrumental del ARSAT-1 y ARSAT-2.
* Cobertura, posicionamiento, bandas de transmisión y servicios.
* Breve descripción de los aparatos de medición montados, para qué sirven y alguna curva o foto con los resultados obtenidos
* Servicios y características de las estaciones terrestres de telecomunicaciones.
* Discusión de la importancia soberanía espacial y de telecomunicaciones en argentina en relación con el manejo de los contenidos y disponibilidad de la información.
* Breve descripción del nuevo ARSAT-3 y tecnologías High-throughput satellite (HTS) y propulsión eléctrica.
* Citar la bibliografía

Se sugiere la siguiente bibliografía para comenzar

(invap)[http://saocom.invap.com.ar/]

(ARSAT)[Plan Satelital argentino (2015-2023)]

(eoportal: SAC-C)[https://directory.eoportal.org/web/eoportal/satellite-missions/s/sac-c]

(eoportal: SAOCOM)[https://directory.eoportal.org/web/eoportal/satellite-missions/s/saocom]

(argentina.gob.ar)[https://www.argentina.gob.ar/ciencia/conae/misiones-espaciales/misiones-cumplidas]

(ARSAT-1 A la altura de las estrellas - Completo)[https://www.youtube.com/watch?v=C6RZOXWDGeU]

(TN Tecno 330-2 Visitamos el data center de ARSAT)[https://www.youtube.com/watch?v=n9e9ad3CyvU]

(COMIENZA EL DESARROLLO DEL ARSAT 3)[https://www.youtube.com/watch?v=-VOmSw2HBTc]

(Visión 7 - Despegue exitoso, el Arsat-2 rumbo al espacio)[https://www.youtube.com/watch?v=b2ILCr3BW6M]

(HTS - High Throughput Satellites)[https://www.youtube.com/watch?v=CW27Uv9W2Bk]
